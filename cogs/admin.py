import discord
from discord.ext import commands

from os import listdir
from os.path import isfile, join
import sys, traceback

import config

class Admin:
    def __init__(self, bot):
        self.bot = bot

        # Настраиваем уникальный цвет всех Embed сообщений для данного модуля
        self.embed_color = discord.Colour(0xff0000)

        # Текущая версия модуля
        self.__version__ = '1.0.0'

    def create_embed(self, ctx, description):
        """ Функция для создания Embed сообщения """

        # Создаем Embed объект
        embed = discord.Embed(title=f'{ctx.command}', colour=self.embed_color, description=description)

        # Устанавливаем footer Embed'a в качестве имени данного класса
        embed.set_footer(text=f'{self.__class__.__name__}, v{self.__version__}')

        # Возвращаем Embed
        return embed

    @commands.group(name='module', help='Управление модулями: включение, выключение, перезапуск и статус.')
    @commands.is_owner()
    async def module_control(self, ctx):
        """ Управление модулями """

        # Если подкоманда не была вызвана
        if ctx.invoked_subcommand is None:

            # Получаем все команды
            commands = ''
            for command in self.module_control.commands:
                commands += f'`{command.name}` - {command.help}\n'

            # Создаем Embed
            embed = self.create_embed(ctx, self.module_control.help)

            # Добавляем в Embed поля с доступными командами и пример использования
            embed.add_field(name="Доступные команды", value=commands)
            embed.add_field(name="Примечание", value='Пример использования: `module load admin`')

            # Отправляем сообщение
            await ctx.send(embed=embed)

    @module_control.command(name='load', help='Команда для включения модуля')
    async def cog_load(self, ctx, cog: str):
        # Инициируем переменную
        embed = None

        try:
            # Пытаемся загрузить cog
            self.bot.load_extension('cogs.' + cog)

        except Exception as e:
            # Выводим traceback в консоль
            traceback.print_exc()

            # Создаем Embed с подробностями ошибки
            embed = self.create_embed(ctx, f'Произошла ошибка при включении модуля `{cog}`')
            embed.add_field(name="Сведения об инциденте", value=f'```{type(e).__name__} - {e}```')

        else:
            # Cog загружен выгружен, создаем Embed
            embed = self.create_embed(ctx, f'Модуль `{cog}` успешно включен')

        # Отправляем сообщение
        await ctx.send(embed=embed)

    @module_control.command(name='unload', help='Команда для выключения модуля')
    async def cog_unload(self, ctx, cog: str):
        # Инициируем переменную
        embed = None

        try:
            # Пытаемся выгрузить cog
            self.bot.unload_extension('cogs.' + cog)

        except Exception as e:
            # Выводим traceback в консоль
            traceback.print_exc()

            # Создаем Embed с подробностями ошибки
            embed = self.create_embed(ctx, f'Произошла ошибка при выключении модуля `{cog}`')
            embed.add_field(name="Сведения об инциденте", value=f'```{type(e).__name__} - {e}```')

        else:
            # Cog успешно выгружен, создаем Embed
            embed = self.create_embed(ctx, f'Модуль `{cog}` выключен')

        # Отправляем сообщение
        await ctx.send(embed=embed)

    @module_control.command(name='reload', help='Команда для перезагрузки модуля')
    async def cog_reload(self, ctx, cog: str):
        # Заранее создаем Embed
        embed = None

        try:
            # Пытаемся выгрузить cog
            self.bot.unload_extension('cogs.' + cog)

            # А теперь пытаемся его загрузить
            self.bot.load_extension('cogs.' + cog)

        except Exception as e:
            # Выводим traceback в консоль
            traceback.print_exc()

            # Создаем Embed с подробностями ошибки
            embed = self.create_embed(ctx, f'Произошла ошибка при перезапуске модуля `{cog}`')
            embed.add_field(name="Сведения об инциденте", value=f'```{type(e).__name__} - {e}```')
        else:
            # Cog успешно перезагружен, создаем Embed
            embed = self.create_embed(ctx, f'Модуль `{cog}` перезапущен')

        # Отправляем сообщение
        await ctx.send(embed=embed)

    @module_control.command(name='status', help='Показать состояние всех модулей')
    async def cog_status(self, ctx):
        # Инициируем переменную со списком всех работающих cog
        loaded_extension = []

        # В цикле добавляем в переменную выше названия cog
        # и приводим к нижнему регистру
        for module_name, var in self.bot.cogs.items():
            loaded_extension.append(module_name.lower())

        # Создаем Embed
        embed = self.create_embed(ctx, 'Состояние модулей')

        # Проверяем директорию с cogs
        for extension in [f.replace('.py', '') for f in listdir(config.discord.get('cogs_directory')) if isfile(join(config.discord.get('cogs_directory'), f))]:

            # Создаем field в Embed, где название - имя cog,
            # а тело - статус данного cog
            embed.add_field(name=f'{extension}', value="Статус: " + ("*Работает*" if extension.lower() in loaded_extension else "__Не работает__"), inline=True)

        # Отправляем сообщение
        await ctx.send(embed=embed)

def setup(bot):
    bot.add_cog(Admin(bot))
