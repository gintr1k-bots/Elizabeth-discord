import json, asyncio

from .feature import *
from .variable import *

async def create_guild(id, connection_pool):
    guild = Guild(id, connection_pool)
    await guild._setup()
    return guild

class Guild:
    id = None
    features = {}

    def __init__(self, id, connection_pool):
        self.id = id
        self.connection_pool = connection_pool

    async def _get_features(self):
        async with self.connection_pool.acquire() as connection:
            async with connection.cursor() as cursor:

                sql = """   SELECT
                                *
                            FROM
                                `features` """

                await cursor.execute(sql)
                return await cursor.fetchall()

    async def _get_variables(self):
        async with self.connection_pool.acquire() as connection:
            async with connection.cursor() as cursor:

                sql = """   SELECT
                                *
                            FROM
                                `variables` """

                await cursor.execute(sql)
                return await cursor.fetchall()

    async def _get_guild_record(self):
        async with self.connection_pool.acquire() as connection:
            async with connection.cursor() as cursor:

                sql = """   SELECT
                                *
                            FROM
                                `guilds`
                            WHERE
                                `id` = %s """
                sql_parameters = (self.id,)

                await cursor.execute(sql, sql_parameters)
                return await cursor.fetchone()

    async def _setup(self):
        guild = await self._get_guild_record()
        features = await self._get_features()
        variables = await self._get_variables()

        if guild is None:
            print(f'Guild {self.id} не найден в БД в таблице `guilds`!')
            return

        have_features = []
        if guild['features'] is not None:
            have_features = json.loads(guild['features'])

        have_variables = []
        if guild['variables'] is not None:
            have_variables = json.loads(guild['variables'])

        for feature in features:

            feature_vars = []
            for variable in variables:
                if variable['feature_id'] != feature['id']:
                    continue

                feature_vars.append(variable)

            self.features[feature['id']] = Feature(data={
                'id': feature['id'],
                'guild': self,
                'name': feature['name'],
                'value': have_features.get(str(feature['id'])),
                'short_description': feature['short_description'],
                'description': feature['description'],
                'enable': feature['enable'],
                'variables': feature_vars,
                'guild_variables': have_variables,
                'connection_pool': self.connection_pool
            })
